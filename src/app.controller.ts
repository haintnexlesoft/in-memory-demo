import { Controller, Get, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { InMemoryDBService } from '@nestjs-addons/in-memory-db';
import { ProductEntity } from './product/entities/product.entity';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private productService: InMemoryDBService<ProductEntity>) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  AddProduct(@Body() product: ProductEntity): ProductEntity {
    return this.productService.create(product);
  }

  @Put()
  EditProduct(@Body() product: ProductEntity) {
    return this.productService.update(product);
  }

  @Delete(':id')
  DeleteProduct(@Param('id') id) {
    return this.productService.delete(id)
  }

  @Get(':id')
  GetProductById(@Param('id') id) {
    return this.productService.query(data => data.id === id)
  }
}
