import { Module } from '@nestjs/common';
import { InMemoryDBModule } from '@nestjs-addons/in-memory-db';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { EmployeeModule } from './employee/employee.module';

@Module({
  imports: [ProductModule, EmployeeModule, InMemoryDBModule.forRoot({})],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
